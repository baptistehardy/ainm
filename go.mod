module gitlab.com/baptistehardy/ainm

go 1.14

require (
	cloud.google.com/go v0.54.0
	github.com/AlecAivazis/survey/v2 v2.0.7
	github.com/briandowns/spinner v1.9.0
	github.com/fatih/color v1.9.0 // indirect
	github.com/mattn/go-colorable v0.1.6 // indirect
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
	golang.org/x/text v0.3.2
	google.golang.org/genproto v0.0.0-20200309141739-5b75447e413d // indirect
)
