package main

import (
	"fmt"
	"github.com/AlecAivazis/survey/v2"
	"github.com/briandowns/spinner"
	"log"
	"os"
	"regexp"
	"time"
)

func main() {
	textToTranslate, _ := TextToTranslatePrompt()

	translateToAllLanguages, _ := TranslateToAllLanguagesPrompt()

	if translateToAllLanguages == false {
		selectedLanguages, _ := SelectLanguagesPrompt()

		for _, selectedLanguage := range selectedLanguages {
			translatedText, err := translateText(selectedLanguage, textToTranslate); if err != nil {
				log.Fatal(err)
			}
			fmt.Printf("%s\n", translatedText)
		}
	} else {
		supportedLanguages, err := listSupportedLanguages(os.Stdout, "en"); if err != nil {
			log.Fatal(err)
		}

		for _, supportedLanguage := range supportedLanguages {
			translatedText, err := translateText(supportedLanguage.Tag.String(), textToTranslate); if err != nil {
				log.Fatal(err)
			}
			fmt.Printf("%s\n", translatedText)
		}
	}
}

func SelectLanguagesPrompt() ([]string, error) {

	s := spinner.New(spinner.CharSets[14], 100*time.Millisecond)
	s.Start()
	langs, err := listSupportedLanguages(os.Stdout, "en"); if err != nil {
		log.Fatal(err)
	}
	s.Stop()

	var supportedLangs []string
	for _, lang := range langs {
		supportedLangs = append(supportedLangs, lang.Name + " (" + lang.Tag.String() + ")")
	}

	prompt := &survey.MultiSelect{
		Message: "Translate to: ",
		Options: supportedLangs,
		PageSize: 10,
	}

	var results []string
	err = survey.AskOne(prompt, &results, survey.WithKeepFilter(true)); if err != nil {
		log.Fatal(err)
	}

	var langCodes []string
	regex := regexp.MustCompile(`\(([^)]+)\)`)

	for _, result := range results {
		stripped := regex.FindString(result)
		langCodes = append(langCodes, stripped[1:len(stripped)-1])
	}

	return langCodes, err
}

func TextToTranslatePrompt() (string, error) {
	result := ""
	prompt := &survey.Input{
		Message: "Text to translate:",
	}
	err := survey.AskOne(prompt, &result)

	return result, err
}

func TranslateToAllLanguagesPrompt() (bool, error) {
	result := true
	prompt := &survey.Confirm{
		Message: "Translate to all available languages? (default to yes)",
		Default: true,
		Help: "Answering 'no' will prompt you to which languages you want to translate that string to.",
	}
	err := survey.AskOne(prompt, &result)

	return result, err
}