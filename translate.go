package main

import (
	"cloud.google.com/go/translate"
	"context"
	"fmt"
	"golang.org/x/text/language"
	"io"
)

func listSupportedLanguages(w io.Writer, targetLanguage string) ([]translate.Language, error) {
	ctx := context.Background()

	lang, err := language.Parse(targetLanguage)
	if err != nil {
		return nil, err
	}

	client, err := translate.NewClient(ctx); if err != nil {
		return nil, err
	}
	defer client.Close()

	langs, err := client.SupportedLanguages(ctx, lang); if err != nil {
		return nil, err
	}

	return langs, nil
}

func translateText(targetLanguage string, text string) (string, error) {
	ctx := context.Background()

	lang, err := language.Parse(targetLanguage)
	if err != nil {
		return "", fmt.Errorf("language.Parse: %v", err)
	}

	client, err := translate.NewClient(ctx)
	if err != nil {
		return "", err
	}
	defer client.Close()

	resp, err := client.Translate(ctx, []string{text}, lang, nil)
	if err != nil {
		return "", fmt.Errorf("Translate: %v", err)
	}
	if len(resp) == 0 {
		return "", fmt.Errorf("Translate returned empty response to text: %s", text)
	}
	return resp[0].Text, nil
}